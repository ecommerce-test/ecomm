World’s largest messaging app i.e., Whatsapp is all set to launch it’s payment service by the end of May as per the Moneycontrol reports. Currently, Whatsapp is working with India’s three Leading banks i.e., HDFC Bank, ICICI Bank, and Axis Bank, from which the beta testing with ICICI Bank has already started. This Facebook leading payment service has been in due from very long despite non-stop beta trials.

Two of the bankers who belong to one of those three banks got aware of the Development of this service, excluding the State Bank of India, who is not participating in its first phase of the rollout.

“We are continuing to work with the government so that we can provide access to payments on WhatsApp to all of our users. Payments on WhatsApp will help accelerate digital payments, and this is particularly important during COVID as it is a safer way to transact for our 400 million users in India,” WhatsApp spokesperson told Moneycontrol.

Previously, Techcrunch report suggested that Facebook has been eyeing on Amazon’s money lending service, which has started a few days back, which named “Pay Later” function, Available for the only limited number of its eligible users. This might be the reason for which they are planning to have its rollout in Priority with the implementation of Lending market space in India. Facebook also came up with a discussion with the National Payments Corporation of India (NPCI) to ensure all regulatory barriers for it’s UPI service.

Not only is Facebook eyeing on its lending service but few more other Payment service companies, Such as Paytm, Mobikwik, Amazon, Flipkart, etc. Has already started it’s service in India as “Pay Later” service, Helping customers, and as well as the Sellers with the credits.

Amazon Pay recently partnered with Capital Float to design and enable ‘Amazon Pay Later‘ service for its eligible customers, and Capital Float has brought in Karur Vysya Bank (KVB) as a co-lending partner to scale up the service.